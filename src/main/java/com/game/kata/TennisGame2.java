package com.game.kata;

import java.util.List;

import static com.game.kata.utils.Constants.*;

/**
 * Tennis game 2.
 */
public class TennisGame2 implements TennisGame {

    private static final List<String> SCORE_LIST = List.of(LOVE, FIFTEEN, THIRTY, FORTY);

    private int p1Point;
    private int p2Point;

    /**
     * Get Score.
     *
     * @return The final score.
     */
    public String getScore() {

        String score = "";
        var firstConditionResult = processFirstCondition(p1Point, p2Point);
        if (firstConditionResult != null) {
            score = firstConditionResult;
        }

        var secondConditionResult = processSecondCondition(p1Point, p2Point);
        if (secondConditionResult != null) {
            score = secondConditionResult;
        }

        var thirdConditionResult = processThirdCondition(p1Point, p2Point);
        if (thirdConditionResult != null) {
            score = thirdConditionResult;
        }

        var lastConditionResult = processLastCondition(p1Point, p2Point);
        if (lastConditionResult != null) {
            score = lastConditionResult;
        }

        return score;
    }

    /**
     * Process the first condition.
     *
     * @param p1Point The P1 Point.
     * @param p2Point The P2 Point.
     *
     * @return The result.
     */
    private static String processFirstCondition(int p1Point, int p2Point) {
        if (p1Point == p2Point) {
            return p1Point < 3 ? SCORE_LIST.get(p1Point) + ALL : DEUCE;
        }
        return null;
    }

    /**
     * Process the second condition.
     *
     * @param p1Point The P1 Point.
     * @param p2Point The P2 Point.
     *
     * @return The result.
     */
    private static String processSecondCondition(int p1Point, int p2Point) {
        if (p1Point > 0 && p1Point < 4 && p2Point == 0) {
            return SCORE_LIST.get(p1Point) + HYPHEN + LOVE;
        }

        if (p2Point > 0 && p2Point < 4 && p1Point == 0) {
            return LOVE + HYPHEN + SCORE_LIST.get(p2Point);
        }
        return null;
    }

    /**
     * Process the third condition.
     *
     * @param p1Point The P1 Point.
     * @param p2Point The P2 Point.
     *
     * @return The result.
     */
    private static String processThirdCondition(int p1Point, int p2Point) {
        if (p1Point > p2Point && p1Point < 4) {
            var p1Res = List.of(2, 3).contains(p1Point) ? SCORE_LIST.get(p1Point) : null;
            var p2Res = List.of(1, 2).contains(p2Point) ? SCORE_LIST.get(p2Point) : null;
            if (p1Res != null && p2Res != null) {
                return p1Res + HYPHEN + p2Res;
            }
        }

        if (p2Point > p1Point && p2Point < 4) {
            var p1Res = List.of(1, 2).contains(p1Point) ? SCORE_LIST.get(p1Point) : null;
            var p2Res = List.of(2, 3).contains(p2Point) ? SCORE_LIST.get(p2Point) : null;
            if (p1Res != null && p2Res != null) {
                return p1Res + HYPHEN + p2Res;
            }
        }
        return null;
    }

    /**
     * Process the last condition.
     *
     * @param p1Point The P1 Point.
     * @param p2Point The P2 Point.
     *
     * @return The result.
     */
    private static String processLastCondition(int p1Point, int p2Point) {
        String score = null;
        if (p1Point > p2Point && p2Point >= 3) {
            score = ADVANTAGE + PLAYER_1;
        }

        if (p2Point > p1Point && p1Point >= 3) {
            score = ADVANTAGE + PLAYER_2;
        }

        if (p1Point >= 4 && p2Point >= 0 && (p1Point - p2Point) >= 2) {
            score = WIN_FOR + PLAYER_1;
        }

        if (p2Point >= 4 && p1Point >= 0 && (p2Point - p1Point) >= 2) {
            score = WIN_FOR + PLAYER_2;
        }
        return score;
    }

    public void p1Score() {
        p1Point++;
    }

    public void p2Score() {
        p2Point++;
    }

    /**
     * Won point.
     *
     * @param player The player name.
     */
    public void wonPoint(String player) {
        if (PLAYER_1.equals(player)) {
            p1Score();
        } else {
            p2Score();
        }
    }
}
