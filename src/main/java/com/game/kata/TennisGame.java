package com.game.kata;

public interface TennisGame {
    void wonPoint(String playerName);
    String getScore();
}
