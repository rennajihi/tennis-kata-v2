package com.game.kata;

import static com.game.kata.utils.Constants.*;

/**
 * Tennis game 1.
 */
public class TennisGame1 implements TennisGame {

    private int mScore1;
    private int mScore2;

    /**
     * Won point.
     *
     * @param playerName The player name.
     */
    public void wonPoint(String playerName) {
        if (PLAYER_1.equals(playerName)) {
            mScore1++;
        } else {
            mScore2++;
        }
    }

    /**
     * Get Score.
     *
     * @return The final score.
     */
    public String getScore() {

        if (mScore1 == mScore2) {
            if (mScore1 >= 3) {
                return DEUCE;
            }
            return processScore(mScore1) + ALL;
        }

        if (mScore1 >= 4 || mScore2 >= 4) {
            return processBigScores(mScore1, mScore2);
        }

        return processScore(mScore1) + HYPHEN + processScore(mScore2);

    }

    /**
     * Process scores when higher than 3.
     *
     * @param mScore1 Score 1
     * @param mScore2 Score 2
     *
     * @return The winner.
     */
    private static String processBigScores(int mScore1, int mScore2) {
        var minusResult = mScore1 - mScore2;

        if (minusResult == 1) {
            return ADVANTAGE + PLAYER_1;
        }

        if (minusResult == -1) {
            return ADVANTAGE + PLAYER_2;
        }

        if (minusResult >= 2) {
            return WIN_FOR + PLAYER_1;
        }

        return WIN_FOR + PLAYER_2;
    }

    /**
     * Process player score.
     *
     * @param score score.
     *
     * @return The score.
     */
    private static String processScore(int score) {
        switch (score) {
            case 0:
                return LOVE;
            case 1:
                return FIFTEEN;
            case 2:
                return THIRTY;
            case 3:
                return FORTY;
            default:
                return NOT_SUPPORTED;
        }
    }
}
